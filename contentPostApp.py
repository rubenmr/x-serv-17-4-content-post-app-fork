#!/usr/bin/python3

"""
webApp class
 Root for hierarchy of classes implementing web applications

 Copyright Jesus M. Gonzalez-Barahona and Gregorio Robles (2009-2015)
 jgb @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - February 2015
"""

import webapp


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        body = request.split('\n')[-1]
        method = request.split(' ', 2)[0]
        resources = request.split(' ', 2)[1]

        return (method,resources,body)

    def process(self, petition):

        method,resources,body = petition

        form = """
            <form action="" method = "POST"> <input type="text" name="name" value="">
            <input type="submit" value="Send"> </form>
        """
        httpCode = "200 OK"
        if method== "GET":

            if resources in self.content.keys():
                htmlBody = "<html><body> Requested: " + resources + " is:  "+ self.content[resources] \
                    + "<br>" + "   New resources? " + form + "</body></html>"
            else:
                httpCode = "404 Not Found"
                htmlBody = "<html><body>Not Found." + "<br>" + "  New resources? :" + form + "</body></html>"
        else:
            self.content[resources] = body
            htmlBody = "<html><body> added " + resources + " with description " + self.content[resources]+ \
                       "   New resources? :" \
                       + "<br>" + form + "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1235)
